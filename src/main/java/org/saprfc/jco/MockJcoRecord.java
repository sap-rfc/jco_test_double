package org.saprfc.jco;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;

import com.sap.conn.jco.ConversionException;
import com.sap.conn.jco.JCoMetaData;
import com.sap.conn.jco.JCoRecord;
import static com.sap.conn.jco.JCoMetaData.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class MockJcoRecord implements JCoRecord{

    private ObjectNode values;
    private Charset AsABAPCharset;
    private JCoMetaData metaData;
    private SimpleDateFormat internalDateFormat = new SimpleDateFormat("yyyy'-'MM'-'dd");
    private SimpleDateFormat internalTimeFormat = new SimpleDateFormat("HH'-'mm'-'ss");
    private SimpleDateFormat externalDateFormat = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat externalTimeFormat = new SimpleDateFormat("HHmmss");

    public MockJcoRecord(ObjectNode values, Charset AsABAPCharset, JCoMetaData metaData){
	this.values = values;
	this.AsABAPCharset = AsABAPCharset;
	this.metaData = metaData;
    }

    public Object clone(){
	return new MockJcoRecord(values, AsABAPCharset, metaData);
    }

    @Override
	public BigDecimal getBigDecimal(int index){
	return getBigDecimal(metaData.getName(index));
    }

    @Override
	public BigDecimal getBigDecimal(String fieldName){
	JsonNode result = values.get(fieldName.toUpperCase());
	int type = metaData.getType(fieldName);
	if(type ==  TYPE_CHAR || type == TYPE_STRING || type == TYPE_NUM){
	    return new BigDecimal(result.asText());
	}else if(type == TYPE_INT1 || type == TYPE_INT2 || type == TYPE_INT ||
		 type == TYPE_BCD || type == TYPE_FLOAT || 
		 type == TYPE_DECF16 || type == TYPE_DECF34){
	    return (BigDecimal)result.numberValue();
	}
	throwConversionException(fieldName);
    }

    @Override
	public BigInteger getBigInteger(int index){
	return getBigDecimal(index).toBigInteger();
    }

    @Override
	public BigInteger getBigInteger(String fieldName){
	return getBigDecimal(fieldName).toBigInteger();
    }

    @Override
	public InputStream getBinaryStream(int index){
	return getBinaryStream(metaData.getName(index));
    }

    @Override
	public InputStream getBinaryStream(String fieldName){
	JsonNode value = values.get(fieldName.toUpperCase());
	int type = metaData.getType(fieldName);
	if(type == TYPE_BYTE || type == TYPE_XSTRING)
	    return new ByteArrayInputStream(value.binaryValue());
	else if(type == TYPE_CHAR  || type == TYPE_STRING)
	    return new ByteArrayInputStream(value.asText().getBytes(AsABAPCharset));
	throwConversionException(fieldName);
    }

    @Override
	public byte getByte(int index){
	return getByte(metaData.getName(index));
    }

    /** conversion is only possible, when char or byte types have a length of one byte or integers < 128 and >= -128 */
    @Override
	public byte getByte(String fieldName){
	JsonNode value = values.get(fieldName.toUpperCase());
	int type = metaData.getType(fieldName);
	byte[] encoded;
	if(type == TYPE_CHAR || type == TYPE_STRING || type == TYPE_NUM){
	    encoded = value.asText().getBytes(AsABAPCharset);
	}else if(type == TYPE_INT1 || type == TYPE_INT2 || type == TYPE_INT){
	    if(value.numberValue().intValue() > 128 || value.numberValue().intValue() < -128)
		throwConversionException(fieldName);
	    encoded = new byte[1];
	    encoded[0] = value.numberValue().byteValue();
	}else if(type == TYPE_BYTE || type == TYPE_XSTRING){
	    encoded = getByteArray(fieldName);
	}else{
	    throwConversionException(fieldName);
	}

	if(encoded.length > 1)
	    throwConversionException(fieldName);
	return encoded[0];
    }

    @Override
	public byte[] getByteArray(int index){
	return getByteArray(metaData.getName(index));
    }

    /** Floats (DEC_F16 and DEC_F34) are not supported in this version */
    @Override
	public byte[] getByteArray(String fieldName){
	JsonNode value = values.get(fieldName.toUpperCase());
	int type = metaData.getType(fieldName);
	byte[] encoded;
	if(type == TYPE_CHAR || type == TYPE_STRING)
	    encoded = value.asText().getBytes(AsABAPCharset);
	else if(type == TYPE_BYTE || type == TYPE_XSTRING)
	    encoded = value.binaryValue();
	else
	    throwConversionException(fieldName);
	return encoded;
    }

    @Override
	public Reader getCharacterStream(int index){
	return getCharacterStream(metaData.getName(index));
    }

    @Override
	public Reader getCharacterStream(String fieldName){
	JsonNode value = values.get(fieldName.toUpperCase());
	int type = metaData.getType(fieldName);
	if(type == TYPE_CHAR || type == TYPE_STRING)
	    return new StringReader(value.asText());
	throwConversionException(fieldName);
    }

    @Override
	public char[] getCharArray(int index){
	return getCharArray(metaData.getName(index));
    }

    /** @returns Date and time fields in the pattern "yyyyMMdd" or "HHmmss" */
    @Override
	public char[] getCharArray(String fieldName){
	JsonNode value = values.get(fieldName.toUpperCase());
	int type = metaData.getType(fieldName);
	if(type == TYPE_CHAR || type == TYPE_STRING || type == TYPE_NUM){
	    return value.asText().toCharArray();
	}else if(type == TYPE_DATE){
	    Date date = internalDateFormat.parse(value.asText(), new ParsePosition(0));
	    return externalDateFormat.format(date).toCharArray();
	}else if(type == TYPE_TIME){
	    Date time = internalTimeFormat.parse(value.asText(), new ParsePosition(0));
	    return externalTimeFormat.format(time).toCharArray();
	}
	throwConversionException(fieldName);
    }

    private void throwConversionException(String fieldName)throws ConversionException{
	throw new ConversionException(new Formatter().format("conversion error for field $1%s of type $2%s", fieldName, metaData.getTypeAsString(fieldName)).toString());
    }

}