# Test double for SAP JCo connector

This library provides a RFC function repository, which is setup from 
local json files. This function repository can be used to mock
RFC calls.

## Create the json files
The json-files can be pragmatically created with the [ABAP debugger 
script 
`zdbgl_script_rec_signature`](https://github.com/germanysources/regression_test).
The scheme looks like:
```json
{
 "function_module":"RFC_GET_SYSTEM_INFO",
 "end_reached": true,
 "declaration":
  {
     "CURRENT_RESOURCES":
     {
       "IS_OPTIONAL":false,
       "IS_IMPORT":false,
       "IS_EXPORT":true,
       "IS_CHANGING":false,
       "IS_TABLE":false,
       "DICTIONARY_TYPE":"SYST-INDEX",
       "KIND":"E"
     },
     ...
  },
  "values":
  {
   "CURRENT_RESOURCES":0,
   ...
  }
}  
```

The directory, in which the json-files are stored, should be provided
at creation of the mocks.

## Restrictions
- ABAP objects are not supported.

## Build
Import Jco connector library into local maven repository:
```shell
mvn install:install-file -Dfile=sapjco3.jar -DgroupId=com.sap.conn.jco -DartifactId=sapjco3 -Dversion=3.0.17 -Dpackaging=jar
```